//
//  Targets.swift
//  SSL Pinning Demo
//
//  Created by Claudio Villanueva on 06-03-23.
//

import Foundation
import Moya

enum Targets {
    case accountBalance
}

// MARK: - TargetType Protocol Implementation
extension Targets: TargetType {
    var baseURL: URL {
        URL(string: "https://ufolabs.cl")!
    }

    var path: String {
        switch self {
        case .accountBalance:
            return "/account_balance.json"
        }
    }

    var method: Moya.Method {
        switch self {
        case .accountBalance:
            return .get
        }
    }

    var task: Moya.Task {
        switch self {
        case .accountBalance:
            return .requestPlain
        }
    }

    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }


}
