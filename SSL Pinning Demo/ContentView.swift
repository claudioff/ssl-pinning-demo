//
//  ContentView.swift
//  SSL Pinning Demo
//
//  Created by Claudio Villanueva on 28-02-23.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var viewModel: ViewModel
    @State var fetchingData = true
    @State var error = ""
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
            }
            Text("Checking account")
                .font(Font.title)
                .foregroundColor(Color.white)
            Spacer()

            if fetchingData {
                LoadingView()
            } else {
                if error.isEmpty {
                    Movements(
                        initialBalance: viewModel.initialBalance,
                        movements: viewModel.movements,
                        finalBalance: viewModel.finalBalance,
                        buttonAction: viewModel.deleteCertificateInKeychain
                    )
                } else {
                    ErrorView(error: error)
                }
            }
        }
        .padding()
        .background(Color.gray)
        .onReceive(viewModel.$fetchingData) { fetchingData in
            print("fetching data: \(fetchingData)")
            self.fetchingData = fetchingData
        }
        .onReceive(viewModel.$error) { error in
            self.error = error
        }
    }
}

struct LoadingView: View {
    var body: some View {
        Spacer()
        ProgressView()
            .progressViewStyle(CircularProgressViewStyle())
            .tint(Color.white)
            .scaleEffect(1.5)
            .padding()
        Text("Loading data...")
            .font(Font.callout)
            .foregroundColor(Color.white)
            .padding()
        Spacer()
        Spacer()
    }
}

struct Movements: View {
    var initialBalance: Double
    var movements: [Movement]
    var finalBalance: Double
    var buttonAction: () -> ()

    var body: some View {
        VStack {
            Group {
                HStack {
                    Spacer()
                }
                Text("Movements")
                    .font(Font.title2)
                    .bold()
                    .padding()

                Divider()

                HStack {
                    Text("Initial balance:")
                        .bold()
                        .padding()
                    Spacer()
                    Text(String(initialBalance))
                        .bold()
                        .padding()
                }

                Divider()

                ForEach(movements, id: \.self) { movement in
                    HStack {
                        Text(movement.type)
                            .padding()
                        Spacer()
                        Text(String(movement.amount))
                            .padding()
                    }
                }

                Divider()

                HStack {
                    Text("Final balance:")
                        .bold()
                        .padding()
                    Spacer()
                    Text(String(finalBalance))
                        .bold()
                        .padding()
                }

                Spacer()
            }

            Button("Delete certificate in keychain", action: {
                buttonAction()
            })

            Spacer()
        }
        .background(Color.white)
        .cornerRadius(15)
    }
}

struct ErrorView: View {
    var error: String

    var body: some View {
        VStack {
            HStack {
                Spacer()
              }
            Spacer()
            Text(error)
                .padding()
            Spacer()
        }
        .background(Color.white)
        .cornerRadius(15)
    }
}
