//
//  NetworkManager.swift
//  SSL Pinning Demo
//
//  Created by Claudio Villanueva on 28-02-23.
//

import Foundation
import Alamofire
import Moya

class NetworkManager {
    static let shared = NetworkManager()

    private enum Constants {
        static let successStatusCodes: Range<Int> = 200..<300
    }

    lazy var certificates: [String: PinnedCertificatesTrustEvaluator] = {
        if KeychainHelper.standard.isCertificateStored() {
            return [
                "ufolabs.cl": PinnedCertificatesTrustEvaluator(
                    certificates: [Certificates.certificate],
                    acceptSelfSignedCertificates: false,
                    performDefaultValidation: true,
                    validateHost: true
                )
            ]
        } else {
            return [:]
        }
    }()

    lazy var serverTrustPolicy: ServerTrustManager = {
        ServerTrustManager(
            allHostsMustBeEvaluated: true,
            evaluators: certificates
        )
    }()

    private lazy var afSession: Session = {
        // Certificate pinning disabled
//        AF
        // Certificate pinning enabled
        Session(serverTrustManager: serverTrustPolicy)
    }()

    private let jsonDecoder = JSONDecoder()

    // MARK: - Alamofire implementation
    func request<R: Decodable>(
        url: String,
        method: HTTPMethod,
        onCompletion: @escaping (R?, ResponseError?) -> Void
    ) {
        afSession.request(
            url,
            method: method,
            encoding: URLEncoding.default
        ).validate(statusCode: Constants.successStatusCodes)
            .responseJSON { [weak self] response in
                switch response.result {
                case .success:
                    do {
                        let decodedResponse = try self!.jsonDecoder.decode(
                            R.self,
                            from: response.data ?? Data()
                        )
                        onCompletion(decodedResponse, nil)
                    } catch {
                        onCompletion(nil, ResponseError(error: ResponseErrorType.decodingError))
                    }
                case .failure(let error):
                    switch error {
                    case .serverTrustEvaluationFailed(let reason):
                        print("serverTrustEvaluationFailed reaseon:\(reason)")
                        onCompletion(nil, ResponseError(error: ResponseErrorType.certificatePinningError))
                    default:
                        onCompletion(nil, ResponseError(error: ResponseErrorType.unknownError))
                    }
                }
            }
    }

    func downloadFile(
        url: String,
        onCompletion: @escaping (URL?) -> Void
    ) {
        let destination: DownloadRequest.Destination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("ufolabs.cer")

            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }

        AF.download(CertificatePinningConstants.certificateUrl, to: destination).response { response in
            debugPrint("downloadFile response:\(response)")
            onCompletion(response.fileURL)
        }
    }

    // MARK: - Moya implementation
    lazy var provider: MoyaProvider<Targets> = {
        MoyaProvider<Targets>(session: afSession)
    }()

    func moyaRequest<R: Decodable>(
        target: Targets,
        onCompletion: @escaping (R?, ResponseError?) -> Void
    ) {
        provider.request(target) { result in
            switch result {
            case let .success(moyaResponse):
                let data = moyaResponse.data
                do {
                    let decodedResponse = try self.jsonDecoder.decode(
                        R.self,
                        from: data
                    )
                    onCompletion(decodedResponse, nil)
                } catch {
                    onCompletion(nil, ResponseError(error: ResponseErrorType.decodingError))
                }

                case let .failure(error):
                print("moyaRequest error:\(error)")
                onCompletion(nil, ResponseError(error: ResponseErrorType.unknownError))
            }
        }
    }
}

enum ResponseErrorType {
    static let certificatePinningError = "Certificate pinning error"
    static let decodingError = "Decoding error"
    static let unknownError = "Unknown error"
    static let noError = "No error"
}

class ResponseError: Codable {
    let error: String

    init(error: String) {
        self.error = error
    }

    init() {
        self.error = ResponseErrorType.noError
    }
}

// MARK: - Certificate pinning
extension NetworkManager {

    func buildEvaluators(evaluator_hosts: [String]) -> [String: ServerTrustEvaluating] {
        var evaluators = [String: ServerTrustEvaluating]()
        for host in evaluator_hosts {
            evaluators[host] = PublicKeysTrustEvaluator()
        }
        return evaluators
    }

    func readLocalFile(fileName name: String, fileExtension: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: fileExtension),
                let data = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return data
            }
        } catch {
            print(error)
        }

        return nil
    }
}
