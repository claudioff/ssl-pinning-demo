//
//  ViewModel.swift
//  SSL Pinning Demo
//
//  Created by Claudio Villanueva on 28-02-23.
//

import Foundation
import Combine

class ViewModel: ObservableObject {
    @Published var initialBalance = 0.0
    @Published var finalBalance = 0.0
    @Published var movements: [Movement] = []

    @Published var fetchingData = true
    @Published var error = ""
    
    init() {
        if KeychainHelper.standard.isCertificateStored() {
            print("Certificate stored in Keychain, retrieving data")
            // To use Alamofire or Moya
//            getDataAlamofire()
            getDataMoya()
        } else {
            print("Preparing certificate")
            prepareCertificate()
        }
    }

    func deleteCertificateInKeychain() {
        KeychainHelper.standard.delete(
            service: CertificatePinningConstants.service,
            account: CertificatePinningConstants.account
        )
        print("Certificate deleted from keychain")
    }

    // MARK: - Alamofire implementation
    func getDataAlamofire() {
        callNetwork { [weak self] (response, error) in
            self?.fetchingData = false

            guard let response else {
                if let error {
                    print("Error:\(error.error)")
                    DispatchQueue.main.async {
                        self?.error = error.error
                    }
                }
                return
            }

            DispatchQueue.main.async {
                self?.initialBalance = response.initialBalance
                self?.movements = response.movements
                self?.finalBalance = response.finalBalance
            }
        }
    }
    
    func callNetwork(onCompletion: @escaping (Balance?, ResponseError?) -> Void) {
        NetworkManager.shared.request(
            url: "https://ufolabs.cl/account_balance.json",
            method: .get,
            onCompletion: { (result, error) in
                onCompletion(result, error)
            }
        )
    }

    // MARK: - Moya implementation
    func getDataMoya() {
        callNetworkMoya { [weak self] (response, error) in
            self?.fetchingData = false

            guard let response else {
                if let error {
                    print("Error:\(error.error)")
                    DispatchQueue.main.async {
                        self?.error = error.error
                    }
                }
                return
            }

            DispatchQueue.main.async {
                self?.initialBalance = response.initialBalance
                self?.movements = response.movements
                self?.finalBalance = response.finalBalance
            }
        }
    }

    func callNetworkMoya(onCompletion: @escaping (Balance?, ResponseError?) -> Void) {
        NetworkManager.shared.moyaRequest(target: Targets.accountBalance) { (response, error) in
            onCompletion(response, error)
        }
    }
}

extension ViewModel {

    func prepareCertificate() {
        if !KeychainHelper.standard.isCertificateStored() {
            downloadCertificateAndStoreInKeychain()
        }
    }

    func getCertificateFromKeychain() -> Data? {
        let data = KeychainHelper.standard.read(
            service: CertificatePinningConstants.service,
            account: CertificatePinningConstants.account
        )

        if let data {
            print("certificate data:\(data)")
        }

        return data
    }

    func downloadCertificateAndStoreInKeychain() {
        NetworkManager.shared.downloadFile(
            url: CertificatePinningConstants.certificateUrl,
            onCompletion: { [weak self] fileURL in
                if let fileURL = fileURL {
                    debugPrint("fileURL:\(fileURL)")
                    KeychainHelper.standard.saveCertificateToKeychain(fileURL: fileURL)
                    do {
                        try FileManager.default.removeItem(at: fileURL)
                    } catch {
                        print("Error removing downloaded certificate:\(error)")
                    }
                    self?.getDataAlamofire()
                }
        })
    }
}
