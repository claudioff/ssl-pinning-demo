//
//  SSL_Pinning_DemoApp.swift
//  SSL Pinning Demo
//
//  Created by Claudio Villanueva on 28-02-23.
//

import SwiftUI

@main
struct SSL_Pinning_DemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: ViewModel())
        }
    }
}
