//
//  Certificates.swift
//  SSL Pinning Demo
//
//  Created by Claudio Villanueva on 03-03-23.
//

import Foundation

struct Certificates {

    /// Get the certificate from keychain
    static let certificate: SecCertificate = Certificates.certificate(
        data: getCertificateFromKeychain() ?? Data()
    )

    /// Returns a certificate as a Data instance to be stored in the keychain
    /// - Parameter filename: Name of the file to open and read
    /// - Returns: A Data instance
    static func certificateAsData(filename: String, fileExtension: String) -> Data? {
        print("filename:\(filename)")
        if let filePath = Bundle.main.path(
            forResource: filename,
            ofType: fileExtension
        ) {
            let data = try! Data(contentsOf: URL(fileURLWithPath: filePath))
            return data
        }
        return nil
    }

    private static func certificate(data: Data) -> SecCertificate {
        return SecCertificateCreateWithData(nil, data as CFData)!
    }

    static func getCertificateFromKeychain() -> Data? {
        KeychainHelper.standard.read(
            service: CertificatePinningConstants.service,
            account: CertificatePinningConstants.account
        )
    }
}
