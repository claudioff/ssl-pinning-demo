//
//  KeychainHelper.swift
//  SSL Pinning Demo
//
//  Created by Claudio Villanueva on 03-03-23.
//

import Foundation

final class KeychainHelper {

    static let standard = KeychainHelper()
    private init() {}

    func save(_ data: Data, service: String, account: String) {

        let query = [
            kSecValueData: data,
            kSecClass: kSecClassGenericPassword,
            kSecAttrService: service,
            kSecAttrAccount: account,
        ] as CFDictionary

        let status = SecItemAdd(query, nil)

        if status == errSecDuplicateItem {
            // Item already exist, thus update it.
            let attributesToUpdate = [kSecValueData: data] as CFDictionary

            // Update existing item
            SecItemUpdate(query, attributesToUpdate)
            print("service:\(service) updated")
        }
    }

    func read(service: String, account: String) -> Data? {

        let query = [
            kSecAttrService: service,
            kSecAttrAccount: account,
            kSecClass: kSecClassGenericPassword,
            kSecReturnData: true
        ] as CFDictionary

        var result: AnyObject?
        SecItemCopyMatching(query, &result)

        return (result as? Data)
    }

    func saveCertificateToKeychain(fileURL: URL) {
        do {
            let data = try Data(contentsOf: fileURL)

            KeychainHelper.standard.save(
                data,
                service: CertificatePinningConstants.service,
                account: CertificatePinningConstants.account
            )
        } catch {
            print("Error loading file: \(fileURL.path)")
        }
    }

    func delete(service: String, account: String) {

        let query = [
            kSecAttrService: service,
            kSecAttrAccount: account,
            kSecClass: kSecClassGenericPassword,
            ] as CFDictionary

        SecItemDelete(query)
    }

    func isCertificateStored() -> Bool {
        let data = KeychainHelper.standard.read(
            service: CertificatePinningConstants.service,
            account: CertificatePinningConstants.account
        )

        guard data != nil else {
            return false
        }

        return true
    }
}

struct CertificatePinningConstants {
    static let service = "certificate_pinning"
    static let account = "certificate_pinning"
    static let fileName = "ufolabs"
    static let fileExtension = "cer"
    static let certificateUrl = "https://ufolabs.cl/ufolabs.cer"
}
