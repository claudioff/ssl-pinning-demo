//
//  Balance.swift
//  SSL Pinning Demo
//
//  Created by Claudio Villanueva on 28-02-23.
//

import Foundation

// MARK: - Balance
struct Balance: Decodable {
    let initialBalance: Double
    let movements: [Movement]
    let finalBalance: Double

    enum CodingKeys: String, CodingKey {
        case initialBalance = "initial_balance"
        case movements
        case finalBalance = "final_balance"
    }
}

// MARK: - Movement
struct Movement: Decodable, Hashable {
    let date: String
    let type: String
    let amount: Double
}
